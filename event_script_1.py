from junos import Junos_Context
from junos import Junos_Trigger_Event
from jnpr.junos import Device
import jcs, paramiko,json
#help("modules")

id = Junos_Trigger_Event.xpath('//trigger-event/id')[0].text
name = Junos_Trigger_Event.xpath('//trigger-event/process/name')[0].text
message = Junos_Trigger_Event.xpath('//trigger-event/message')[0].text

def createSSHClient(server, port, user, password, src, dst):
	client = paramiko.SSHClient()
	client.load_system_host_keys()
	client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
	print (" Connecting to %s \n with user=%s... \n" %(server,user))
	t = paramiko.Transport(server, port)
	t.connect(username=user,password=password)
	sftp = paramiko.SFTPClient.from_transport(t)
	print(t)
	print ("Copying file: %s to path: %s" %(src, dst))
	sftp.put(src, dst)
	sftp.close()
	t.close() 
	return client

def main():
	jdev = Device().open()
	js ='{"id":"'+id+'","name":"'+name+'" ,"message":"'+message+'","get_system_information":'+json.dumps(jdev.rpc.get_system_information())+'}'
	jdev.close()
	f = open("/var/log/aaaaa_log_event_script1.json","w+")
	f.write(json.dumps(json.loads(js)))
	f.close();
	ssh = createSSHClient('172.16.12.38', 22, 'root', 'scanda21','/var/log/aaaaa_log_event_script1.json','/root/aaaaa_log_event_script1.json')

if __name__ == '__main__':
	main()