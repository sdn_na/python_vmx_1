# Python 3
from jnpr.junos import Device
from getpass import getpass
import sys
from lxml import etree
from jnpr.junos.utils.config import Config

hostname = input("Device hostname: ")
junos_username = input("Junos OS username: ")
junos_password = getpass("Junos OS password: ")
junos_interface = input("Interface Name: ")
junos_unit = input("Unit: ")
junos_address = input("Interface Address: ")


try: 
    # NETCONF session over SSH
    dev = Device(host=hostname, user=junos_username, passwd=junos_password).open()
    
    with Config(dev, mode='exclusive') as cu:
        cu.load('set interfaces '+junos_interface+' unit '+junos_unit+' family inet address '+junos_address, format='set')
        cu.pdiff()
        cu.commit()

except Exception as err:
    print (err)
    sys.exit(1)