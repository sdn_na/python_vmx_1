from napalm import get_network_driver
from getpass import getpass
import sys
from pprint import pprint

hostname = input("Device hostname: ")
junos_username = input("Junos OS username: ")
junos_password = getpass("Junos OS password: ")


try: 
    driver = get_network_driver('junos')
    device = driver(hostname, junos_username, junos_password)
    device.open()
    device.load_merge_candidate(filename='napalm_config_files/hostname_config.txt')
    print(device.compare_config())
    device.commit_config()
except Exception as err:
    print (err)
    sys.exit(1)