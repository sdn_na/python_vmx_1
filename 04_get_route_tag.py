# Python 3
from jnpr.junos import Device
from getpass import getpass
import sys
from lxml import etree

hostname = input("Device hostname: ")
junos_username = input("Junos OS username: ")
junos_password = getpass("Junos OS password: ")


try: 
    # NETCONF session over SSH
    with Device(host=hostname, user=junos_username, passwd=junos_password) as dev:
        print (dev.display_xml_rpc('show route', format='text'))
    
except Exception as err:
    print (err)
    sys.exit(1)