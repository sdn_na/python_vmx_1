from napalm import get_network_driver
from getpass import getpass
import sys
from pprint import pprint

hostname = input("Device hostname: ")
username = input("FortiOS username: ")
password = getpass("FortiOS password: ")

try: 
    driver = get_network_driver('fortios')
    device = driver(hostname, username, password)
    device.open()
    pprint(device.get_facts())
except Exception as err:
    print (err)
    sys.exit(1)