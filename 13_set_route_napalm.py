from napalm import get_network_driver
from getpass import getpass
import sys
from pprint import pprint

hostname = input("Device hostname: ")
junos_username = input("Junos OS username: ")
junos_password = getpass("Junos OS password: ")
junos_dest = input("Destination: ")
junos_next_hop = input("Next-hop: ")

def change_configuration(device, configuration):
  device.load_merge_candidate(config=configuration)
  print(device.compare_config())
  device.commit_config()


try: 
    driver = get_network_driver('junos')
    device = driver(hostname, junos_username, junos_password)
    device.open()
    change_configuration(device,
    'routing-options { static { route '+junos_dest+' next-hop '+junos_next_hop+'; } }'
    )
except Exception as err:
    print (err)
    sys.exit(1)