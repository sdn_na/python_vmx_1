from napalm import get_network_driver
from getpass import getpass
import sys
from pprint import pprint

hostname = input("Device hostname: ")
junos_username = input("Junos OS username: ")
junos_password = getpass("Junos OS password: ")
junos_interface = input("Interface Name: ")
junos_unit = input("Unit: ")
junos_address = input("Interface Address: ")

def change_configuration(device, configuration):
  device.load_merge_candidate(config=configuration)
  print(device.compare_config())
  device.commit_config()


try: 
    driver = get_network_driver('junos')
    device = driver(hostname, junos_username, junos_password)
    device.open()
    change_configuration(device,
    'interfaces { '+junos_interface+' { unit '+junos_unit+' { family inet { address '+junos_address+'; } } } } '
    )
except Exception as err:
    print (err)
    sys.exit(1)