from napalm import get_network_driver
from getpass import getpass
import sys
from pprint import pprint

hostname = input("Device hostname: ")
username = input("FortiOS username: ")
password = getpass("FotiOS password: ")


try: 
    driver = get_network_driver('fortios')
    device = driver(hostname, username, password)
    device.open()
    device._load_config(filename="napalm_config_files/set_hostname_fortios.j2", config=None)
    print(device.compare_config())
    device.commit_config()
    device.close()
    
except Exception as err:
    print (err)
    sys.exit(1)
