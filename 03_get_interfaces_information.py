# Python 3
from jnpr.junos import Device
from getpass import getpass
import sys
from lxml import etree

hostname = input("Device hostname: ")
junos_username = input("Junos OS username: ")
junos_password = getpass("Junos OS password: ")

try: 
    # NETCONF session over SSH
    with Device(host=hostname, user=junos_username, passwd=junos_password) as dev:
        sw_info_text = dev.rpc.get_interface_information(terse=True)
        print('------')
        print(etree.tostring(sw_info_text))  
        print('------') 
        for element in sw_info_text:
            print('******')
            print(etree.tostring(element))
            print('******')
            print('-.-.-.')
            print(etree.tostring(element[0]))
            print(element.find('name').text)
            print(element.find('name').text.strip())
            print('-.-.-.')
            print('+++++++')
            for sub_element in element:
                print(etree.tostring(sub_element))
            print('+++++++')
        
    
except Exception as err:
    print (err)
    sys.exit(1)