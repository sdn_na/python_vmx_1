# Python 3
from jnpr.junos import Device
from getpass import getpass
import sys
from lxml import etree
from jnpr.junos.utils.config import Config

hostname = input("Device hostname: ")
junos_username = input("Junos OS username: ")
junos_password = getpass("Junos OS password: ")
junos_dest = input("Destination: ")
junos_next_hop = input("Next-hop: ")


try: 
    # NETCONF session over SSH
    dev = Device(host=hostname, user=junos_username, passwd=junos_password).open()
    
    with Config(dev, mode='exclusive') as cu:
        cu.load('set routing-options static route '+junos_dest+' next-hop '+junos_next_hop, format='set')
        cu.pdiff()
        cu.commit()

except Exception as err:
    print (err)
    sys.exit(1)