from napalm import get_network_driver
from getpass import getpass
import sys
from pprint import pprint

hostname = input("Device hostname: ")
junos_username = input("Junos OS username: ")
junos_password = getpass("Junos OS password: ")

try: 
    driver = get_network_driver('junos')
    device = driver(hostname, junos_username, junos_password)
    device.open()
    config_sample=device.get_config()
    print(config_sample['running'])
except Exception as err:
    print (err)
    sys.exit(1)